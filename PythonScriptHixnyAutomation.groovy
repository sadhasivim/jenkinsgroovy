import com.gsihealth.jenkins.Common
import com.gsihealth.jenkins.pojo.GsiJob
import com.gsihealth.jenkins.pojo.GsiRun
import com.gsihealth.jenkins.runner.GitBuildTask
import com.gsihealth.jenkins.utils.CommonUtils
import com.gsihealth.jenkins.utils.EmailListBuilder
import com.gsihealth.jenkins.utils.GitBuildSummary
import com.gsihealth.jenkins.utils.Logger
def call(body) {
Logger logger = new Logger()
def config = [:]
body.resolveStrategy = Closure.DELEGATE_FIRST
body.delegate = config
body()
// def disableEmail=config.disableEmail
// def currentjobPath
// def failure = false
// def status = false
def checkoutpath = config.checkoutpath
def gitRepoURL=config.gitRepoURL
// def reportpath= config.reportpath
def testsuitename = config.testsuitename
def projectWorkspace

node{
projectWorkspace = pwd()
GitBuildTask task = new GitBuildTask(env)
task.setConfig(config)
task.startDate = new Date()
System.setProperty("hudson.model.DirectoryBrowserSupport.CSP", "")
dir("${checkoutpath}"){
	deleteDir()
}
//1st Stage
stage('Checkout from BitBucket'){
logger.info("[BUILD START]")
dir("${checkoutpath}") {
def checkoutResult = git credentialsId: 'cf7a0040-05b2-4835-b49e-8e5814141f5b',branch: "${config.branch}", url: "${config.gitRepoURL}", changeLog:true
task.checkoutResult = checkoutResult 
sleep(10)
bat 'cd..'
}
}

stage('Test Execution'){
try{
dir("${checkoutpath}/TestLoader"){
	pythoncmd = "python ${testsuitename}.py "
	logger.info("${pythoncmd}")
	//pip install part
	// bat "python -m pip install --upgrade pip"
	// bat "pip install html-testRunner"
	// bat "pip install markupsafe"
	// bat "pip install Faker"
	// bat "pip install beautifulsoup4"
	// bat "pip install bs4"
	// bat "pip install docutils"
	// bat "pip install fpdf"
	// bat "pip install lxml"
	// bat "pip install jmespath"
	// bat "pip install mysql-connector-python"
	// bat "pip install names"
	// bat "pip install pytest"
	// bat "pip install selenium"
	// bat "pip install setuptools"
	// bat "pip install soupsieve"
	bat "${pythoncmd}"
}
}
catch(e){
print "The Python error is ${e}"
echo "[BUILD END]"
// status = false
currentBuild.result="SUCCESS"
}
}
// import java.text.SimpleDateFormat
// def date = new Date()
// sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss")

stage('Publish Report'){
dir("${checkoutpath}"){
def todaydate = new Date().format( 'yyyy-MM-dd' )
print "${todaydate}"
def fileDir = new File("${checkoutpath}/reports").listFiles()
def folderName="${fileDir}"
publishHTML(target: [
allowMissing: false,
alwaysLinkToLastBuild: false,
keepAll: true,
reportDir: "${checkoutpath}/Temp",
reportFiles: "*.html",
reportName: "Html Report"
])
}

// if(disableEmail){
// try{

// def jobPath = "${env.JENKINS_HOME}"
// jobPath = getJobPath(jobPath, env.JOB_NAME)
// def consolePath = jobPath+ "\\builds\\${env.BUILD_NUMBER}\\log"
// sleep 5
// echo "reading the console log..."
// def log = readFile(consolePath)
// def buildLog =  lastMatch(log, /\[BUILD START\]([\s|\S]*)\[BUILD END\]/)
// if(buildLog){
// buildLog=replaceAll(buildLog,/\[8mh.+\[Pipeline\](?:(?!\r\n)[\s|\S])*\r\n/,"")
// buildLog=replaceAll(buildLog,/\[8mh.+\[Pipeline\]/,"")      
// }
// def DEFAULT_CONTENT

// if("${buildLog} && ${buildLog}.length()!=0"){
// DEFAULT_CONTENT="<br>=========CONSOLE LOG=========<br><br><pre>${buildLog}</pre>"
// }else{
// DEFAULT_CONTENT="Sauce Auto Deployment Job Script execution failed for ${env.JOB_NAME}"
// }


// if (status){    
// emailext attachmentsPattern: '**/CustomizedReports/*.html', body: "${currentBuild.result}: ${env.BUILD_URL}", subject: "Build Notification: ${JOB_NAME}-Build# ${BUILD_NUMBER} ${currentBuild.result}", to: 'prabhaharan.velu@gsihealth.com,Dinesh.Netaji@gsihealth.com'          
// }else{
// emailext attachmentsPattern: '**/CustomizedReports/*.html', body: "${currentBuild.result}: ${env.BUILD_URL}", subject: "Build Notification: ${JOB_NAME}-Build# ${BUILD_NUMBER} ${currentBuild.result}", to: 'prabhaharan.velu@gsihealth.com,Dinesh.Netaji@gsihealth.com'
// }

// }catch(e){
// echo "[BUILD END]"
// print e
// emailext body: "Script has some error! ${e}", subject: "Jenkins warning:${env.JOB_NAME} Build ${currentBuild.displayName}", to: 'prabhaharan.velu@gsihealth.com'
// }
// }
}
// //5th Stage
// stage('Push the reports to BitBucket'){
// dir("${reportpath}") {    
// bat 'git config --global --replace-all user.name "Sadha Sivim"'
// bat 'git config --global --replace-all user.email "sadha.sivim@gsihealth.com"'
// bat 'git pull -f origin master'
// bat 'git add -A'
// def gitcmd="git commit -m \"${config.testSuitePath}_Result_Update\""
// bat "${gitcmd}"                
// bat 'git push -f origin HEAD:master'
// }
// }
}
}
// @NonCPS
// def getJobPath(rootPath, job_name) {
// def arr = job_name.split("/")
// for (int i = 0; i < arr.length; i++) {
// rootPath += "\\jobs\\${arr[i]}"
// }
// return rootPath
// }

// @NonCPS
// def String lastMatch(text, regex) {
// def m;
// if ((m = text =~ regex)) {
// return m[0][-1]
// }
// return ''
// }

// @NonCPS
// def String replaceAll( text, regex, newText) {
// return text.replaceAll(regex, newText)
// }


// @NonCPS
// def String getPSCmd(command){
// def prefix = "powershell.exe -command \""
// def suffix = "\";exit \$LASTEXITCODE;"
// return prefix+command+suffix
// }