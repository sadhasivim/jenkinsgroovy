import com.gsihealth.jenkins.Common
import hudson.model.ParameterValue
import hudson.model.ParametersAction
import hudson.model.Result
import jenkins.model.Jenkins
import groovy.json.JsonOutput
import groovy.json.JsonSlurperClassic
import com.gsihealth.jenkins.pojo.GsiJob
import com.gsihealth.jenkins.pojo.GsiRun
import com.gsihealth.jenkins.runner.GitBuildTask
import com.gsihealth.jenkins.utils.CommonUtils
import com.gsihealth.jenkins.utils.EmailListBuilder
import com.gsihealth.jenkins.utils.GitBuildSummary
import com.gsihealth.jenkins.utils.Logger
import static groovy.io.FileType.FILES


def call(body) {


    Logger logger = new Logger()

    // evaluate the body block, and collect configuration into the object
    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()
    def failure = false
	def ip = config.ip
	def nifiVersion = config.nifiVersion
	def environment = config.env.toUpperCase()
	
	//CONSTANTS

	BASH_FILE_LOCATION = "D:/jenkins_utils/deployment_v2/bash"
	//BASH_FILE_LOCATION = "C:/Temp/bash"
	NIFI_RESTART_SCRIPT_PATH = "D:/jenkins_utils/deployment_v2/bash/NifiRestart.sh"
	PSCP_PATH = "D:\\PuTTY\\pscp.exe"
	JENKINS_WORKSPACE = "D:\\Jenkins\\workspace"
	COPY_PROPS_FILE_SCRIPT_PATH = "D:/jenkins_utils/deployment_v2/bash/CopyPropertyFile2NiFiConf.sh"
	COPY_WSDL_FILES_SCRIPT_PATH = "D:/jenkins_utils/deployment_v2/bash/CopyWsdlFiles2NiFiConf.sh"
	
	NIFI_PROPERTIES = [
		DEV: [
			BOXES: ['devzk01', 'devzk02', 'devzk03'],
			PROPS_FILE_NAME: 'nifi-dev.properties',
			WSDL_FOLDER_NAME: 'wsdl',
			USER: 'cdnonprod',
			USER_PASSWORD: 'P@ssw0rd1'
		],
		TEST: [
			BOXES: ['10.168.54.67', '10.168.54.68', '10.168.54.69'],
			PROPS_FILE_NAME: 'nifi-qa.properties',
			WSDL_FOLDER_NAME: 'wsdl',
			USER: 'cdnonprod',
			USER_PASSWORD: 'P@ssw0rd1'
		],		
		STAGE: [
			BOXES: ['stgnifi01.madc.local', 'stgnifi02.madc.local', 'stgnifi03.madc.local'],
			PROPS_FILE_NAME: 'nifi-stage.properties',
			WSDL_FOLDER_NAME: 'wsdl',
			USER: 'cdnonprod',
			USER_PASSWORD: 'P@ssw0rd1'
		],				
	]
	

    node {

        GitBuildTask task = new GitBuildTask(env)
        task.setConfig(config)
        task.startDate = new Date()
        def pathAppend = config.path?:""


        dir("./gsiflow-properties") {
			 stage('nifi Config Import'){
                def checkoutResult = git credentialsId: '60c19ca5-84bb-4508-a665-0c7895d3031b', url: "https://sadhasivim@bitbucket.org/gsihealth/gsiflow-properties.git", changeLog:true
                task.checkoutResult = checkoutResult
				}
		}
		
		stage('Copying NiFi Properties File and Restarting NiFi'){ 
			println("running in ${environment} environment")
			try {
				if (!NIFI_PROPERTIES.containsKey(environment)) throw new Exception("Not a known NiFi environment")
				copyPropsFile2NiFi(NIFI_PROPERTIES[environment].BOXES, NIFI_PROPERTIES[environment].PROPS_FILE_NAME, NIFI_PROPERTIES[environment].USER, NIFI_PROPERTIES[environment].USER_PASSWORD)
				copyWsdlFiles2NiFi(NIFI_PROPERTIES[environment].BOXES, NIFI_PROPERTIES[environment].WSDL_FOLDER_NAME, NIFI_PROPERTIES[environment].USER, NIFI_PROPERTIES[environment].USER_PASSWORD)
				restartNiFi(NIFI_PROPERTIES[environment].BOXES, NIFI_PROPERTIES[environment].USER, NIFI_PROPERTIES[environment].USER_PASSWORD)
			}
			catch(e){
				print e
				currentBuild.result = Result.FAILURE
				throw e
			}
		}

		/*
        stage('Post Build'){
               
			   logger.info("Email stage...")

                try{
                    logger.info("build completed.")
                    task.endDate = new Date()
                    task.duration = CommonUtils.getTimeDelta(task.endDate, task.startDate)
                    task.result = currentBuild.result?:"SUCCESS"

                    currentBuild.displayName = "#${env.BUILD_NUMBER}_${config.release}"
                    currentBuild.description = "Revision: ${task.checkoutResult.GIT_COMMIT}"

                    def requested = CommonUtils.getRequestedUser(steps)
                    def culprits = CommonUtils.getCulprits(steps)
                    def developers = CommonUtils.getDevelopers(steps)
                    def to = new EmailListBuilder(steps)
                        .requestedUser()
                        .culprits()
                        .developers()
                        .CDTeam()
                        .DevLead()
                        .build()
                    task.setDevelopers(null)
                    task.setRequestedUser(requested)
                    task.setCulprits(null)

                    logger.info("Started by: ${requested}")
                    logger.info("Responsible Developers: ${culprits}")
                    logger.info("Developers: ${developers}")

                    logger.info("to: ${to}")

                    task.setRun(new GsiRun(
                            parent: new GsiJob(name: env.JOB_NAME),
                            displayName: currentBuild.displayName,
                            number: currentBuild.number
                        )
                    )

                    GitBuildSummary buildSummary = new GitBuildSummary(steps, env, task)

                    logger.info(task)

                    if(!config.disableEmail){
                        buildSummary.sendEmail(to, true)
                    }

                }catch(e){
                    print e
                    emailext body: "Script has some error! ${e}", subject: "Jenkins warning:${env.JOB_NAME} Build ${currentBuild.displayName}", to: 'ContinuousDelivery@gsihealth.com'
                }

            }
			*/

    }


}

def copyPropsFile2NiFi(nifiNodeList, propsFileName, user, password){
	nifiNodeList.each {
		//String remoteCommand = "echo y | D:\\PuTTY\\pscp.exe -pw P@ssw0rd1 -r D:\\Jenkins\\workspace\\${env.JOB_NAME}\\gsiflow-properties\\nifi-stage.properties cdnonprod@stgnifi01.madc.local:/home/cdnonprod"
		String remoteCommand = "echo y | ${PSCP_PATH} -pw ${password} -r ${JENKINS_WORKSPACE}\\${env.JOB_NAME}\\gsiflow-properties\\${propsFileName} ${user}@${it}:/home/${user}"
		bat "${remoteCommand}"  
		
		//setaccessCommandQa = "echo y | plink cdnonprod@stgnifi01.madc.local -pw P@ssw0rd1 -m D:\\jenkins_utils\\deployment_v2\\bash\\CopyPropertyFile2NiFiConf.sh"
		remoteCommand = "echo y | plink ${user}@${it} -pw ${password} -m ${COPY_PROPS_FILE_SCRIPT_PATH}"
		bat "${remoteCommand}"
	}
}

def copyWsdlFiles2NiFi(nifiNodeList, wsdlFolderName, user, password){
	nifiNodeList.each {
		String remoteCommand = "echo y | ${PSCP_PATH} -pw ${password} -r ${JENKINS_WORKSPACE}\\${env.JOB_NAME}\\gsiflow-properties\\${wsdlFolderName} ${user}@${it}:/home/${user}"
		bat "${remoteCommand}"  
		
		remoteCommand = "echo y | plink ${user}@${it} -pw ${password} -m ${COPY_WSDL_FILES_SCRIPT_PATH}"
		bat "${remoteCommand}"
	}
}

def  restartNiFi(nifiNodeList, user, password){
	nifiNodeList.each {
		String remoteCommandRestartNiFi = "echo y | plink ${user}@${it} -pw ${password} -m ${NIFI_RESTART_SCRIPT_PATH}"
		println("remoteCommandRestartNiFi : ${remoteCommandRestartNiFi}")
		bat "${remoteCommandRestartNiFi}"  
	}
}
